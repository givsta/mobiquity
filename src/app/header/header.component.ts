import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  // Set nav links and labels
  navLinks = [
    { path: '', label: 'F1 Championship' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
