import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ChampionshipService } from '../championship.service';
import { MatSort, MatTableDataSource, MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material';

@Component({
  selector: 'app-championship-winners',
  templateUrl: './championship-winners.component.html',
  styleUrls: ['./championship-winners.component.scss']
})
export class ChampionshipWinnersComponent implements OnInit {

  winners = [];
  worldChampion = null;
  constructorLogoUrl: string;

  // Set isLoading to false as we haven't made an http call.
  isLoading = false;

  // Create a variable that contains the list of the columns to be rendered.
  displayedColumns: string[] = ['driver', 'constructor', 'race', 'date'];

  // Assign the data to the data source for the table to render.
  dataSource = new MatTableDataSource(this.winners);

  // Use @ViewChild decorator to inject MatSort.
  @ViewChild(MatSort) sort: MatSort;
  
  // The constructor is called before any other lifecycle hook, this is where we can inject dependencies.
  // Access the injected data (season/year) using the MAT_BOTTOM_SHEET_DATA injection token.
  // Inject the ChampionshipService.
  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any, public championshipService: ChampionshipService) {}

  // Call this after the constructor to initialize the component's content.
  ngOnInit() {

    // Call function to get the world champion for the selected season/year.
    this.getWorldChampion(this.data.championshipYear);

    // Call function to get the winners of all races in the selected season/year.
    this.getWinnersForAllRacesInYear(this.data.championshipYear);
  }

  getWorldChampion(season) {
    // The service method returns an Observable of data.
    // The component has to subscribe to the methods return data.
    /**
     * The subscription callback copies the data fields into the component's worldChampion object,
     * which is data-bound in the component template for display.
     */
    this.championshipService.getWorldChampion(season)
    .subscribe((data: any) => {
      this.worldChampion = data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
    });
  }

  getWinnersForAllRacesInYear(season) {
    // Let the user know that we're making an http request and the system is not hanging/stuck.
    this.isLoading = true;

    this.championshipService.getWinnersForEachRace(this.data.championshipYear)
    .subscribe((data: any) => {

      // Request is complete, loading image is hidden.
      this.isLoading = false;
      this.winners = data.MRData.RaceTable.Races;

      // Set dataSource for the winners table.
      this.dataSource.data = this.winners;

      // Allow the data to be sorted.
      this.dataSource.sort = this.sort;
    });
  }

  /* Function that gets the constructor name as a parameter then assigns the 
   * respective constructor logo and returns the image path.
   */
  getConstructorLogo(constructorName) {
    const imagePath = '../../assets/images/constructors/';

    switch(constructorName) {
      case 'Ferrari':
        this.constructorLogoUrl = imagePath + 'ferrari.jpg';
        break;
      case 'Force Inida':
        this.constructorLogoUrl = imagePath + 'forceindia.jpg';
        break;
      case 'Haas':
        this.constructorLogoUrl = imagePath + 'haas.jpg';
        break;
      case 'Honda':
        this.constructorLogoUrl = imagePath + 'honda.jpg';
        break;
      case 'Lotus F1':
        this.constructorLogoUrl = imagePath + 'lotus.jpg';
        break;
      case 'McLaren':
        this.constructorLogoUrl = imagePath + 'mclaren.jpg';
        break;
      case 'Mercedes':
        this.constructorLogoUrl = imagePath + 'mercedes.jpg';
        break;
      case 'Red Bull':
        this.constructorLogoUrl = imagePath + 'redbull.jpg';
        break;
      case 'Renault':
        this.constructorLogoUrl = imagePath + 'renault.jpg';
        break;
      case 'BMW Sauber':
        this.constructorLogoUrl = imagePath + 'sauber.jpg';
        break;
      case 'Toro Rosso':
        this.constructorLogoUrl = imagePath + 'tororosso.jpg';
        break;
      case 'Williams':
        this.constructorLogoUrl = imagePath + 'williams.jpg';
        break;
      case 'Brawn':
        this.constructorLogoUrl = imagePath + 'brawngp.jpg';
        break;
      default:
        // default placeholder image is set if no image is available.
        this.constructorLogoUrl = imagePath + 'placeholder.jpg';
        break;
    }

    // return the image path.
    return this.constructorLogoUrl;

  }
}
