import { Component, OnInit, ViewChild } from '@angular/core';
import { ChampionshipService } from '../championship.service';
import { MatSort, MatTableDataSource, MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { ChampionshipWinnersComponent } from '../championship-winners/championship-winners.component';

@Component({
  selector: 'app-championship-list',
  templateUrl: './championship-list.component.html',
  styleUrls: ['./championship-list.component.scss']
})
export class ChampionshipListComponent implements OnInit {

  championships = [];
  isLoading = false;

  // Create a variable that contains the list of the columns to be rendered.
  displayedColumns: string[] = ['icon', 'season', 'actions'];

  // Assign the data to the data source for the table to render
  dataSource = new MatTableDataSource(this.championships);

  // Use @ViewChild decorator to inject MatSort directive.
  @ViewChild(MatSort) sort: MatSort;

  constructor(private bottomSheet: MatBottomSheet, public championshipService: ChampionshipService) {}

  // Call this after the constructor to initialize the component's content.
  ngOnInit() {
    this.isLoading = true;

    this.championshipService.getChampionships()
    .subscribe((data: any) => {
      this.isLoading = false;
      this.championships = data.MRData.SeasonTable.Seasons;

      // Provide data to by passing a data array
      this.dataSource.data = this.championships; 

      setTimeout(() => {
        // Allow data to be sorted
        this.dataSource.sort = this.sort
        this.dataSource.sortingDataAccessor = (data, header) => data[header];
      });
    });
  }

  // Click event handler method, which passes the clicked season/year from the template into the method.
  openBottomSheet(year): void {
    // Pass in the season/year to the bottom sheet using the data property.
    this.bottomSheet.open(ChampionshipWinnersComponent, {
      data: { championshipYear: year }
    });
  }
}
