import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({ providedIn: "root" })
export class ChampionshipService {

  constructor(private http: HttpClient) {}

  getChampionships() {
    // GET request to get championships 2005-2015
    return this.http.get('https://ergast.com/api/f1/seasons.json?limit=11&offset=55');
  }

  getWinnersForEachRace(year: string) {
    // GET request to get winners for each race in specific season/year
    return this.http.get('https://ergast.com/api/f1/' + year + '/results/1.json');
  }

  getWorldChampion(year: string) {
    // GET request to get the world champion of a specific season/year
    return this.http.get('https://ergast.com/api/f1/' + year + '/driverStandings/1.json');
  }
}
