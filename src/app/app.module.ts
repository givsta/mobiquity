import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatIconRegistry,
  MatTableModule,
  MatTabsModule,
  MatSortModule,
  MatBottomSheetModule
} from "@angular/material";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { ChampionshipListComponent } from "./championship/championship-list/championship-list.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { ChampionshipWinnersComponent } from './championship/championship-winners/championship-winners.component';
import { FooterComponent } from './footer/footer.component';

// Define app module
@NgModule({

  // List of components that belong to this module.
  declarations: [
    AppComponent,
    HeaderComponent,
    ChampionshipListComponent,
    ChampionshipWinnersComponent,
    FooterComponent
  ],
  // List of modules to import into this module.
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatBottomSheetModule,
    HttpClientModule
  ],
  // List of dependency injection providers visible both to the contents of this module and to importers of this module.
  providers: [
    MatIconRegistry
  ],
  // List of components to bootstrap when this module is bootstrapped.
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public MatIconRegistry: MatIconRegistry) {
      MatIconRegistry.registerFontClassAlias('fontawesome', 'fa');
    }
}
