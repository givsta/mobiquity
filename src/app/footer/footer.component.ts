import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  // Define path for frontend tech logos
  frontendLogoPath = '../../assets/images/logos/';

  frontendTechnologies = [
    { imagePath: this.frontendLogoPath + 'html.png', name: 'HTML 5' },
    { imagePath: this.frontendLogoPath + 'css.png', name: 'CSS 3' },
    { imagePath: this.frontendLogoPath + 'js.png', name: 'JavaScript' },
    { imagePath: this.frontendLogoPath + 'angular.png', name: 'Angular IO' },
    { imagePath: this.frontendLogoPath + 'typescript.png', name: 'TypeScript' },
    { imagePath: this.frontendLogoPath + 'sass.png', name: 'Sass' }
  ];

  constructor() { }

  ngOnInit() {

  }

}
