import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ChampionshipListComponent } from "./championship/championship-list/championship-list.component";
import { ChampionshipWinnersComponent } from "./championship/championship-winners/championship-winners.component";

// configure routes for the application
const routes: Routes = [
    { path: '', component: ChampionshipListComponent },
    { path: '', component: ChampionshipWinnersComponent }
];

@NgModule ({
    // list of modules to import into this module
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}