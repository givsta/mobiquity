Angular 6 App
=======================

### [See the Demo][demo]!
---------------------

This project serves as a working example of a demo
application consuming the [Ergast Developer API][ergast] and using [Angular][angular] as the frontend framework.

### Desktop Screenshots

![Screenshot1](https://givsta.bitbucket.io/assets/images/bitbucket/landing.png)
![Screenshot2](https://givsta.bitbucket.io/assets/images/bitbucket/winners.png)

### Mobile Screenshots

![Mobile](https://givsta.bitbucket.io/assets/images/bitbucket/mobile.jpg)

Getting Started
----------------

To get you started you can simply clone the repository and install the dependencies:

### Prerequisites

You need git to clone the repository. You can get git from [here][git].

Node.js is required to initialize the project. You must have [Node.js][node]
and its package manager ([npm][npm]) installed. 

### Clone `mobiquity`

Clone the `mobiquity` repository using git:

```
git clone https://givsta@bitbucket.org/givsta/mobiquity.git
cd formula-one
```

### Install Dependencies

There are a couple of dependencies in this project which help manage the project that we need.

* We get the tools we depend upon via `npm`, the [Node package manager][npm].

You may simply do:

```
npm install
```

After that, you should find that you have a new folder in your project.

* `node_modules` - contains the npm packages for the tools we need

### Run the Application

The project is preconfigured with a simple development web server. To be able to test on mobile too please get your IP address using either of the following:

```
Windows - type ipconfig in Commmand Prompt 
Mac - type ifconfig |grep inet in Terminal
```

All you have to do next is run:

```
ng serve --host <your IP address here>.
```

The app should be available on any device on the same network.

Now browse to the app at **<your IP address>:4200/** e.g. **http://192.168.0.10:4200/**

### What's under the hood?

The project is built using:

* HTML 5
* CSS 3
* JavaScript
* Angular
* Angular Material
* Sass

Upon running the app, an HTTP request to `http://ergast.com/api/f1/seasons?limit=11&offset=55` is made to 
get seasons/championships between 2005-2015.

Clicking **eye icon** (in the action column) on a row invokes the bottom sheet that requests all the winners for every race for the selected year from `http://ergast.com/api/f1/2015/results/1.json`

When a winner is a world champion in the same year, there a highlighted green bar next to their name to show this. This request is made using `https://ergast.com/api/f1/2015/driverStandings/1.json` 

`.json` is used to return *JSON* data that the app can use.

Clicking away from the sheet will dismiss the bottom sheet.

A user is also given the ability to sort the seasons and drivers by date on the respective tables.

### Thank you :-)

[demo]: https://givsta.bitbucket.io/
[ergast]: http://ergast.com/mrd/
[angular]: https://angular.io/
[node]: https://nodejs.org/
[npm]: https://www.npmjs.org/
[git]: https://git-scm.com/